%   CHE 5753 – Applied Numerical Computing for Scientists and Engineers
%               *Computational Assignment 3: ODE Solver*
%______________________________________________________________________%
% Created by: Haden Kolmer                                    
%             Oklahoma State University                              
%             Petroleum Engineering Master's
%             haden.kolmer@okstate.edu (please email for issues or
%                                           suggestions)
%_________________________________________________________________________%
%% Problem %%
% This assignment has two parts: 1.) generate a MATLAB file and 
% 2.) create a .html file through MATLAB publisher. The goal for the 
% assignment is to solve a series of ODEs from a given equation sheet and 
%given initial conditions to produce the required plots.

%% Executing solve_ODEs %%
function solve_ODEs_CA3

%% Input Parameters and Constants %%

%clear all variable for program start
clear variables; clc

%Input data
%defining parameter values
Vspan = [0 1]; %Volumetric bounds 
V1 = 0;        %initial volume
T = 423;       %Celsius
Fa = 100;
Fb = 0;
Fc = 0;

%Constants
Ct0 = 0.1;
Cpa = 90;  %J/mol
Cpb = 90;  %J/mol
Cpc = 180;  %J/mol
Ua = 4000;  %J/(m^3*s*C)
Ta = 373;  %Celsius
E1overR = 4000;  %Kelvin
E2overR = 9000;  %Kelvin
Hr1a = -20000;  %J/(mol of A reacted in reaction 1)
Hr2a = -60000;  %J/(mol of A reacted in reaction 2)
T0 = 423;       %celsius

%Creating the initial condition and parameter matrix
ic = [423, 100, 0, 0];
parameters = [E1overR, E2overR, Ct0, Cpa, Cpb, Cpc, Ta, Hr1a, Hr2a, T0];

%% Plots and Figures %%
        
        %Temperature profile plot
        figure
        [x,y]=ode45(@(V,y) ODEs_CA3(V,y,parameters),Vspan,ic);
        plot(x,y(:,1),'-','linewidth',1.1)
        grid on
        xlabel('V (dm^3)')
        ylabel('T (K)')
        title('Temperature Profile')
        legend('Temperature')
        
        
        %Molar flow rate plot
        figure
        [x,y]=ode45(@(V,y) ODEs_CA3(V,y,parameters),Vspan,ic);
        plot(x,y(:,2),'-g',x,y(:,3),'--r',x,y(:,4),'-b', 'linewidth',1.1)
        grid on
        xlabel('V (dm^3)')
        ylabel('Fi (mol/s)')
        title('Profile of Molar Flow Rates Fa, Fb, and Fc')
        legend('Fa','Fb', 'Fc')
        
%% Function definition and equations %%

function output = ODEs_CA3(V, y, parameters)

    %Parameter locations in parameter matrix
    E1overR = parameters(1);
    E2overR = parameters(2);
    Ct0 = parameters(3);
    Cpa = parameters(4);
    Cpb = parameters(5);
    Cpc = parameters(6);
    Ta = parameters(7);
    Hr1a = parameters(8);
    Hr2a = parameters(9);
    T0 = parameters(10);
    
    %Output matrix locations
    T = y(1);
    Fa = y(2);
    Fb = y(3);
    Fc = y(4);

    %Stoichiometry E12-5.10 thru E12-5.13
    Ft = Fa + Fb + Fc;
    Ca = Ct0 * (Fa / Ft) * (T0 / T);
    Cb = Ct0 * (Fb / Ft) * (T0 / T);
    Cc = Ct0 * (Fc / Ft) * (T0 / T);

    %Expression for k1a and k2a
    k1a = 10 * exp((E1overR * ((1 / 300) - (1 / T))));
    k2a = 0.09 * exp((E2overR * ((1 / 300) - (1 / T))));

    %Rate laws E12-5.1 thru E12-5.2
    r1a = -k1a * Ca;
    r2a = -k2a * (Ca ^ 2);

    %Net Rates E12-5.7 thru E12-5.9
    ra = r1a + r2a;
    rb = k1a * Ca;
    rc = 0.5 * k2a * (Ca ^ 2);

    %Defining mole balances E12-5.4 thru E12-5.6
    dFadV = ra;  %molar flow rate of A (mol/s)
    dFbdV = rb;  %molar flow rate of B (mol/s)
    dFcdV = rc;  %molar flow rate of C (mol/s)

    %PFR Energy balance E12-5.3
    dTdV = (Ua * (Ta - T) + (-r1a * -Hr1a) + (-r2a * -Hr2a)) / (Fa * Cpa + Fb * Cpb + Fc * Cpc);

    %Output parameters for plots
    output = [dTdV; dFadV; dFbdV; dFcdV];
    
           
end
end

