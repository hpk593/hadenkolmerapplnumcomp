"""
   CHE 5753 – Applied Numerical Computing for Scientists and Engineers
               *Computational Assignment 4: FCC 3 lump model*
______________________________________________________________________%
 Created by: Haden Kolmer using Dr. Ford Versypt's ODE Param Estim EX2
             Oklahoma State University
             Petroleum Engineering Master's
             haden.kolmer@okstate.edu (please Email for issues or
                                           suggestions)
_________________________________________________________________________%
% Problem %%
 This assignment has two parts: 1.) generate a Python file and
 2.) document using Jupyter creating a .ipynb file. The goal for the
 assignment is to solve a series of ODEs from a given equation sheet and
 given initial conditions to produce the required plots.

"""

# ---------------------------------------------------------------------------

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# Data for 3 lump model
xaxisData = np.array([0.01667, 0.03333, 0.05, 0.1])  # time, independent variable
yaxisData = np.array([[0.5074, 0.3796, 0.2882, 0.1762], [0.3767, 0.4385, 0.4865, 0.5416],
                      [.1159, 0.1819, 0.2253, 0.2822]])  # x, dependent variable - VGO, Gasoline, Gas + Coke

# guesses for parameters
k1guess = 1.0
k2guess = 1.0
k3guess = 1.0
parameterguesses = np.array([k1guess, k2guess, k3guess])


# Need two functions for our model
# 1. to define the system of ODE(s)
# 2. to solve the ODE(s) and return ypredicted values in same shape as yaxisData

# 1. define ODEs
def system_of_ODEs(y, t, parameters):  # yvar, xvar, args
    # unpack the parameters
    k1 = parameters[0]
    k2 = parameters[1]
    k3 = parameters[2]
    # unpack the dependent variables
    y1 = y[0]
    y2 = y[1]
    y3 = y[2]
    # differential equations for 3 lump model
    dy1dt = -(k1 + k3) * y1 ** 2
    dy2dt = k1 * y1 ** 2 - k2 * y2
    dy3dt = k3 * y1 ** 2 + k2 * y2

    return dy1dt, dy2dt, dy3dt


# end of function

# 2. Solve ODEs at xaxisData points
# and return calculated yaxisCalculated
# using current values of the parameters
def model(xaxisData, *params):
    # initial condition(s) for the ODE(s)
    yaxis0 = np.array([1.0, 0.0, 0.0])  # should include a decimal
    # new for > 1 dependent variables:
    numYaxisVariables = 3
    yaxisCalc = np.zeros((xaxisData.size, numYaxisVariables))

    for i in np.arange(0, len(xaxisData)):
        if xaxisData[i] == 0.0:  # should include a decimal
            # edit for > 1 dependent variables:
            yaxisCalc[i, :] = yaxis0
        else:
            xaxisSpan = np.linspace(0.0, xaxisData[i], 101)
            ySoln = odeint(system_of_ODEs, yaxis0, xaxisSpan, args=(params,))  # soln for entire xaxisSpan
            # edit for > 1 dependent variables:
            yaxisCalc[i, :] = ySoln[-1, :]  # calculated y at the end of the xaxisSpan
            # at this point yaxisCalc is now 2D matrix with the number of columns set as : to include all yvariables
            # curve_fit needs a 1D vector that has the rows in a certain order, which result from the next two commands
    yaxisOutput = np.transpose(yaxisCalc)
    yaxisOutput = np.ravel(yaxisOutput)
    return yaxisOutput
    # end of for loop


# end of model function

# Estimate the parameters
parametersoln, pcov = curve_fit(model, xaxisData, np.ravel(yaxisData), p0=parameterguesses)
print(parametersoln)
# edit for > 1 dependent variables:
xdataConversion = 1 - yaxisData[0, :]

# plotting raw data for VGO, Gasoline, Gas+Coke
plt.plot(xdataConversion, yaxisData[0, :], 'bs')
plt.plot(xdataConversion, yaxisData[1, :], 'yx')
plt.plot(xdataConversion, yaxisData[2, :], 'ro')
plt.legend(['VGO', 'Gasoline', 'Gas+Coke'])

# initial condition(s) for the ODE(s)
yaxis0 = np.array([1.0, 0.0, 0.0])  # should include a decimal
numYaxisVariables = 3
xaxisForPlotting = np.linspace(0, xaxisData[-1], 101)

# plot for yield v conversion
yaxisCalc_OptionB = odeint(system_of_ODEs, yaxis0, xaxisForPlotting, args=(parametersoln,))
Conversion = 1 - yaxisCalc_OptionB[:, 0]  # updating conversion and plotting

# plotting best fit lines
plt.plot(Conversion, yaxisCalc_OptionB[:, 0], 'b-', label='y1 fitted')
plt.plot(Conversion, yaxisCalc_OptionB[:, 1], 'y-', label='y2 fitted')
plt.plot(Conversion, yaxisCalc_OptionB[:, 2], 'r-', label='y3 fitted')
plt.xlabel('conversion')
plt.ylabel('yield')
plt.show()

# plot for yield v time
# plotting raw data for VGO, Gasoline, Gas+Coke
plt.plot(xaxisData, yaxisData[0, :], 'bs')
plt.plot(xaxisData, yaxisData[1, :], 'yx')
plt.plot(xaxisData, yaxisData[2, :], 'ro')
plt.legend(['VGO', 'Gasoline', 'Gas+Coke'])
plt.legend(['VGO', 'Gasoline', 'Gas+Coke'])

# plotting best fit lines
plt.plot(xaxisForPlotting, yaxisCalc_OptionB[:, 0], 'b-', label='y1 fitted')
plt.plot(xaxisForPlotting, yaxisCalc_OptionB[:, 1], 'y-', label='y2 fitted')
plt.plot(xaxisForPlotting, yaxisCalc_OptionB[:, 2], 'r-', label='y3 fitted')
plt.xlabel('time')
plt.ylabel('yield')
plt.xlim(0, 0.1)
plt.show()
