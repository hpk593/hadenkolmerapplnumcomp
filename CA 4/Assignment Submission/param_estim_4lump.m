%   CHE 5753 – Applied Numerical Computing for Scientists and Engineers
%               *Computational Assignment 4: FCC 4 lump model*
%______________________________________________________________________%
% Created by: Haden Kolmer using Dr. Ford Versypt's L12example2.m                                    
%             Oklahoma State University                              
%             Petroleum Engineering Master's
%             haden.kolmer@okstate.edu (please email for issues or
%                                           suggestions)
%_________________________________________________________________________%
%% Problem 
% This assignment has two parts: 1.) generate a MATLAB file and 
% 2.) create a .html file through MATLAB publisher. The goal for the 
% assignment is to solve a series of ODEs from a given equation sheet and 
% given initial conditions to produce the required plots.

%% Input Data and Guesses 
function param_estim_4lump()

% Clear all variable for program start
clf
clear all

tdata = [0.01667 0.0333 0.0500 0.1000]; % independent variable, x-axis (hrs)
y1data = [0.5074 0.3796 0.2882 0.1762]; % dependent variables, y-axis - VGO
y2data = [0.3767 0.4385 0.4865 0.5416]; % dependent variables, y-axis - Gasoline
y3data = [0.0885 0.136 0.1681 0.2108]; % dependent variables, y-axis - Gas
y4data = [0.0274 0.0459 0.0572 0.0714]; % dependent variables, y-axis - Coke

ydata = [y1data;y2data;y3data;y4data];

conversion = 1.- y1data;

y0 = [1 0 0 0]; % initial conditions

% Parameter guesses
k12guess = 1;
k13guess = 1;
k14guess = 1;
k23guess = 1;
k24guess = 1;

parameterguesses = [k12guess, k13guess, k14guess, k23guess, k24guess];

% Estimate k parameters
parameters = lsqcurvefit(@(parameterguesses,tdata)model(parameterguesses,tdata,y0),parameterguesses, tdata, ydata)

%% Plotting 

% Plot inputs
tforplotting = linspace(0.0001,tdata(end),101);
timespan = linspace(0,1,101);
xatguesses = model(parameterguesses, tforplotting,y0);
xatsoln = model(parameters,tforplotting,y0);
xaxisConversion = 1 - xatsoln(1,:);

% Plot for conversion
figure(1)
plot(xaxisConversion, xatsoln(1,:), 'y', xaxisConversion,xatsoln(2,:),'b',xaxisConversion,xatsoln(3,:),'r',xaxisConversion,xatsoln(4,:),'g')
hold on 
plot(conversion, ydata(1,:), 'ys', conversion,ydata(2,:),'bs',conversion,ydata(3,:),'rx',conversion,ydata(4,:),'go')
legend('VGO','Gasoline', 'Gas', 'Coke')
xlim([0,1])
ylim([0,1])
xlabel('Conversion, wt fraction')
ylabel('Yield, wt fraction')

% Plot for time
figure(2)
plot(tforplotting,xatsoln(1,:),'y',tforplotting,xatsoln(2,:),'b',tforplotting,xatsoln(3,:),'r',tforplotting,xatsoln(4,:),'g')
hold on 
plot(tdata,ydata(1,:),'ys',tdata,ydata(2,:),'bs',tdata,ydata(3,:),'rx',tdata,ydata(4,:),'go')
legend('VGO','Gasoline', 'Gas', 'Coke')
xlim([0,0.1])
ylim([0,1])
xlabel('Time')
ylabel('Yield, wt fraction')

%% ODE Solver and Model Display 
function output = model(parameters,t,y0)
    for i = 1:length(t)
        if t(i) == 0 
            tsoln = 0;
            ysoln = y0;
            output(:,i) = ysoln;
        else
            tspan = [0 t(i)]; 
            [tsoln, ysoln] = ode23s(@(t,y) system_of_ODEs(t,y,parameters), tspan, y0);
            output(i,:) = ysoln(end,:);
        end
    end
    output = output';
end

function output = system_of_ODEs(t,y,parameters)
    % Unpack parameters
    k12 = parameters(1);
    k13 = parameters(2);
    k14 = parameters(3);
    k23 = parameters(4);
    k24 = parameters(5);
    
    % Locate y1, y2, y3, and y4
    y1 = y(1);
    y2 = y(2);
    y3 = y(3);
    y4 = y(4);
    
    % ODE for 4 lump
    dydt(1) = -(k12+k13+k14)*y1^2;
    dydt(2) = k12*y1^2 - k23*y2 - k24*y2;
    dydt(3) = k13*y1^2 + k23*y2;
    dydt(4) = k14*y1^2 + k24*y2;
    
    output = dydt';
end



end